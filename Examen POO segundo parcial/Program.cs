﻿using System;

namespace Examen_POO_segundo_parcial
{
    class Program
    {
        static void Main(string[] args)
        {
            var conexion = new Conexion();

            //Queremos crear una cadena de conexion al SQL server
            conexion.nuevaConexion(new SqlServidorBuilder());
            conexion.crearCadenaConexion();
            var conexionSQLServer = conexion.ConexionLista;

            //Queremos crear una cadena de conexion a MySQL
            conexion.nuevaConexion(new MySqlBuilder());
            conexion.crearCadenaConexion();
            var conexionMySQL = conexion.ConexionLista;

            //Queremos crear una cadena de conexion a ORACLE
            conexion.nuevaConexion(new OracleBuilder());
            conexion.crearCadenaConexion();
            var conexionOracle = conexion.ConexionLista;
        }
    }
}
