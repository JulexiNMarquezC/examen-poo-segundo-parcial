﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Examen_POO_segundo_parcial
{
    public class SqlServidorBuilder: ConexBuil 
    {
        //Creamos un objeto cadena conexion para luego sobreescribirlo
        //Esto con todas las constructor
        public SqlServidorBuilder()
        {
            conexion = new CadenaConex();
        }
        //Usamos la clase override para sobre escribir la clase inicial 
        public override void asignarservidor()
        {
            conexion.Servidor = "Servidor SQL server";
        }
        public override void asignarBaseDatos()
        {
            conexion.NombreDeBase = "Nombre instancia SQL server";
        }
        public override void asignarUsuario()
        {
            conexion.Usuario = "Usuario SQL server";
        }
        public override void asignarcontrasena()
        {
            conexion.Contrasena= "Contraseña de usuario SQL server";
        }
    }
}
