﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Examen_POO_segundo_parcial
{
    public class Conexion
    {
        private ConexBuil ConexBuil;
        public void nuevaConexion(ConexBuil conexBuilder)
        {
            ConexBuil = conexBuilder;
        }
        public void crearCadenaConexion()
        {
            ConexBuil.asignarservidor();
            ConexBuil.asignarBaseDatos();
            ConexBuil.asignarcontrasena();
            ConexBuil.asignarUsuario();
        }

        public CadenaConex ConexionLista
        {
            get { return ConexBuil.cadenaConex(); }
        }

    }
}
