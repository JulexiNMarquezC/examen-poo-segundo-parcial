﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Examen_POO_segundo_parcial
{
    //Este es el objeto que se contruira varias veces
    public class CadenaConex
    {
        public string Servidor { get; set;}
        public string NombreDeBase { get; set; }
        public string Usuario { get; set; }
        public string Contrasena { get; set; }
        public CadenaConex()
        {

        }
        public CadenaConex(string servidor, string baseDatos, string usuario, string contra): this() 
        {
            Servidor = servidor;
            NombreDeBase = baseDatos;
            Usuario = usuario;
            Contrasena = contra; 
        }
    }
}
