﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Examen_POO_segundo_parcial
{
    public class MySqlBuilder: ConexBuil
    {
        public MySqlBuilder()
        {
            conexion = new CadenaConex();
        }
        public override void asignarservidor()
        {
            conexion.Servidor = "Servidor MySQL";
        }
        public override void asignarBaseDatos()
        {
            conexion.NombreDeBase = "Nombre instancia MySQL";
        }
        public override void asignarUsuario()
        {
            conexion.Usuario = "Usuario MySQL";
        }
        public override void asignarcontrasena()
        {
            conexion.Contrasena = "Contraseña del Usuario de MySQL";
        }
    }
}
