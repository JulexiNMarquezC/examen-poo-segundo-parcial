﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Examen_POO_segundo_parcial
{
    public class OracleBuilder : ConexBuil 
    {
        public OracleBuilder()
        {
            conexion = new CadenaConex();
        }
        public override void asignarservidor()
        {
            conexion.Servidor = "Servidor ORACLE";
        }
        public override void asignarBaseDatos()
        {
            conexion.NombreDeBase = "Nombre instancia ORACLE";
        }
        public override void asignarUsuario()
        {
            conexion.Usuario = "Usuario ORACLE";
        }
        public override void asignarcontrasena()
        {
            conexion.Contrasena = "Contraseña del Usuario de ORACLE";
        }
    }
}
