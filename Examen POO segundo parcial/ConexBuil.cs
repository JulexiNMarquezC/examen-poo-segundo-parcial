﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Examen_POO_segundo_parcial
{
    //Builder
    public abstract class ConexBuil
    {
        protected CadenaConex conexion;
        public CadenaConex cadenaConex() { return conexion; }
        //Aqui asignamos valores a las propiedades
        //En estas se pueden añadir otros métodos como insertar datos
        public virtual void asignarservidor()
        { 

        }
        public virtual void asignarBaseDatos() 
        { 

        }
        public virtual void asignarUsuario() 
        { 

        }
        public virtual void asignarcontrasena() 
        {

        }
    }
}
